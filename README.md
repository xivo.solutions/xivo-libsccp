# xivo-libsccp

An Asterisk channel driver for the SCCP protocol.

This work is based on the Asterisk chan\_skinny originally written by Jeremy
McNamara & Florian Overkamp.

## Getting Started

* https://documentation.xivo.solutions/en/stable/xivo/contributors/sccp.html
* https://gitlab.com/xivo.solutions/xivo-libsccp

## Authors

* XiVO development team
* Nicolas Bouliane
